define('suitecommerce_inventorydisplay.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var t = {"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "  <div itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\">\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"isInStock") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "  </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "    <link itemprop=\"availability\" href=\"http://schema.org/InStock\"/>\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "    <link itemprop=\"availability\" href=\"http://schema.org/OutOfStock\"/>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var helper;

  return "  "
    + container.escapeExpression(((helper = (helper = compilerNameLookup(helpers,"loadingText") || (depth0 != null ? compilerNameLookup(depth0,"loadingText") : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"loadingText","hash":{},"data":data}) : helper)))
    + "\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"stockMessage") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "  <div class=\"inventory-display\">\n    <p class=\"inventory-display-stock-information-"
    + alias4(((helper = (helper = compilerNameLookup(helpers,"messageType") || (depth0 != null ? compilerNameLookup(depth0,"messageType") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageType","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = compilerNameLookup(helpers,"orderType") || (depth0 != null ? compilerNameLookup(depth0,"orderType") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"orderType","hash":{},"data":data}) : helper)))
    + " inventory-display-message-text\">\n        <span class=\"inventory-display-stock-information-"
    + alias4(((helper = (helper = compilerNameLookup(helpers,"messageType") || (depth0 != null ? compilerNameLookup(depth0,"messageType") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageType","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = compilerNameLookup(helpers,"orderType") || (depth0 != null ? compilerNameLookup(depth0,"orderType") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"orderType","hash":{},"data":data}) : helper)))
    + " icon\"><i></i></span>\n        <span class=\"inventory-display-message-"
    + alias4(((helper = (helper = compilerNameLookup(helpers,"messageType") || (depth0 != null ? compilerNameLookup(depth0,"messageType") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageType","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = compilerNameLookup(helpers,"orderType") || (depth0 != null ? compilerNameLookup(depth0,"orderType") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"orderType","hash":{},"data":data}) : helper)))
    + "\"> "
    + alias4(((helper = (helper = compilerNameLookup(helpers,"stockMessage") || (depth0 != null ? compilerNameLookup(depth0,"stockMessage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"stockMessage","hash":{},"data":data}) : helper)))
    + "</span>\n    </p>\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"quantityAvailableMessage") : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    var helper;

  return "      <p class=\"inventory-display-quantity-available\">\n        <span>"
    + container.escapeExpression(((helper = (helper = compilerNameLookup(helpers,"quantityAvailableMessage") || (depth0 != null ? compilerNameLookup(depth0,"quantityAvailableMessage") : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"quantityAvailableMessage","hash":{},"data":data}) : helper)))
    + "</span>\n      </p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = compilerNameLookup(helpers,"unless").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"isBackorderable") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"isLoading") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.program(8, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true}; var main = t.main; t.main = function(){ arguments[1] = arguments[1] || {}; var ctx = arguments[1]; ctx._extension_path = 'http://localhost:7777/tmp/extensions/SuiteCommerce/InventoryDisplay/1.1.0/'; ctx._theme_path = 'http://localhost:7777/tmp/extensions/TavanoTeam/Manor/1.0.32/'; return main.apply(this, arguments); }; var template = Handlebars.template(t); template.Name = 'suitecommerce_inventorydisplay'; return template;});