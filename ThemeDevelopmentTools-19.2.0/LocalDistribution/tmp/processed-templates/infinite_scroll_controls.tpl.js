define('infinite_scroll_controls.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var t = {"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"isButtonBottom") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "      <div class=\"col-md-3\">\n      </div>\n      <div class=\"col-md-9\">\n        <div class=\"infinite-scroll-btn-holder arrow-down\">\n            <a data-action=\"load-pages\" class=\"infinite-scroll-btn-load-page arrow-down\" id=\""
    + alias4(((helper = (helper = compilerNameLookup(helpers,"buttonID") || (depth0 != null ? compilerNameLookup(depth0,"buttonID") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonID","hash":{},"data":data}) : helper)))
    + "\">\n                <p>"
    + alias4(((helper = (helper = compilerNameLookup(helpers,"buttonText") || (depth0 != null ? compilerNameLookup(depth0,"buttonText") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonText","hash":{},"data":data}) : helper)))
    + "<p>\n                <i class=\"icon-chevron-down arrow-down\"></i>\n            </a>\n        </div>\n      </div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <div class=\"infinite-scroll-btn-holder arrow-up\">\n          <a data-action=\"load-pages\" class=\"infinite-scroll-btn-load-page arrow-up\" id=\""
    + alias4(((helper = (helper = compilerNameLookup(helpers,"buttonID") || (depth0 != null ? compilerNameLookup(depth0,"buttonID") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonID","hash":{},"data":data}) : helper)))
    + "\">\n              <i class=\"icon-chevron-up arrow-up\"></i>\n              <p>"
    + alias4(((helper = (helper = compilerNameLookup(helpers,"buttonText") || (depth0 != null ? compilerNameLookup(depth0,"buttonText") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonText","hash":{},"data":data}) : helper)))
    + "<p>\n          </a>\n        </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"showButton") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true}; var main = t.main; t.main = function(){ arguments[1] = arguments[1] || {}; var ctx = arguments[1]; ctx._extension_path = 'http://localhost:7777/tmp/extensions/SuiteCommerce/InfiniteScrollExtension/1.0.3/'; ctx._theme_path = 'http://localhost:7777/tmp/extensions/TavanoTeam/Manor/1.0.32/'; return main.apply(this, arguments); }; var template = Handlebars.template(t); template.Name = 'infinite_scroll_controls'; return template;});