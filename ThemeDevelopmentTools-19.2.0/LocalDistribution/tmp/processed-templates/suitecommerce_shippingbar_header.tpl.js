define('suitecommerce_shippingbar_header.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var t = {"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"isFloating") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        <div class=\"sc-shippingbar-header-background-transition-base sc-shippingbar-header-background sc-shippingbar-header-background"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"shippingGoalAvailable") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + " "
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"isFloating") : depth0),{"name":"if","hash":{},"fn":container.program(37, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " sc-shippingbar-header-message-"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"multipleMessages") : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.program(12, data, 0),"data":data})) != null ? stack1 : "")
    + "\">\n            <div class=\"sc-shippingbar-header\">\n                <div class=\"sc-shippingbar-header-row-wrapper\">\n                    <div class=\"sc-shippingbar-header-container-row\">\n                        <div class=\"col-md-1 col-sm-1 col-xs-1 sc-shippingbar-header-chevron-container\">\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"multipleMessages") : depth0),{"name":"if","hash":{},"fn":container.program(39, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                        </div>\n                        <div class=\""
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"isFloating") : depth0),{"name":"if","hash":{},"fn":container.program(16, data, 0),"inverse":container.program(18, data, 0),"data":data})) != null ? stack1 : "")
    + " sc-shippingbar-header-text-center\">\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"shippingGoalAvailable") : depth0),{"name":"if","hash":{},"fn":container.program(41, data, 0),"inverse":container.program(46, data, 0),"data":data})) != null ? stack1 : "")
    + "                        </div>\n                        <div class=\"col-md-1 col-sm-1 col-xs-1 sc-shippingbar-header-chevron-container\">\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"multipleMessages") : depth0),{"name":"if","hash":{},"fn":container.program(54, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                        </div>\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"isFloating") : depth0),{"name":"if","hash":{},"fn":container.program(56, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </div>\n                </div>\n            </div>\n        </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "            <!--googleoff: index-->\n                <div class=\"sc-shippingbar-header-background-transition-base sc-shippingbar-header-background sc-shippingbar-header-background"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"shippingGoalAvailable") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + " sc-shippingbar-header-message-"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"multipleMessages") : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.program(12, data, 0),"data":data})) != null ? stack1 : "")
    + "\">\n                    <div class=\"sc-shippingbar-header\">\n                        <div class=\"sc-shippingbar-header-row-wrapper\">\n                            <div class=\"sc-shippingbar-header-container-row\">\n                                <div class=\"col-md-1 col-sm-1 col-xs-1 sc-shippingbar-header-chevron-container\">\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"multipleMessages") : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                                </div>\n                                <div class=\""
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"isFloating") : depth0),{"name":"if","hash":{},"fn":container.program(16, data, 0),"inverse":container.program(18, data, 0),"data":data})) != null ? stack1 : "")
    + " sc-shippingbar-header-text-center\">\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"shippingGoalAvailable") : depth0),{"name":"if","hash":{},"fn":container.program(20, data, 0),"inverse":container.program(25, data, 0),"data":data})) != null ? stack1 : "")
    + "                                </div>\n                                <div class=\"col-md-1 col-sm-1 col-xs-1 sc-shippingbar-header-chevron-container\">\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"multipleMessages") : depth0),{"name":"if","hash":{},"fn":container.program(33, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                                </div>\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(alias1,(depth0 != null ? compilerNameLookup(depth0,"isFloating") : depth0),{"name":"if","hash":{},"fn":container.program(35, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                            </div>\n                        </div>\n                    </div>\n                </div>\n            <!--googleon: index-->\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "-congratulations";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"isInProgress") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.program(8, data, 0),"data":data})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    return "-progress";
},"8":function(container,depth0,helpers,partials,data) {
    var helper;

  return "-initial-"
    + container.escapeExpression(((helper = (helper = compilerNameLookup(helpers,"currentMessageIndex") || (depth0 != null ? compilerNameLookup(depth0,"currentMessageIndex") : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"currentMessageIndex","hash":{},"data":data}) : helper)));
},"10":function(container,depth0,helpers,partials,data) {
    return "multi";
},"12":function(container,depth0,helpers,partials,data) {
    return "single";
},"14":function(container,depth0,helpers,partials,data) {
    return "                                        <i class=\"sc-shippingbar-header-chevron-left\" data-action=\"previous-text\"></i>\n";
},"16":function(container,depth0,helpers,partials,data) {
    return "col-md-9 col-sm-9 col-xs-9";
},"18":function(container,depth0,helpers,partials,data) {
    return "col-md-10 col-sm-10 col-xs-10";
},"20":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"haveLink") : depth0),{"name":"if","hash":{},"fn":container.program(21, data, 0),"inverse":container.program(23, data, 0),"data":data})) != null ? stack1 : "");
},"21":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                                            <a a href="
    + alias4(((helper = (helper = compilerNameLookup(helpers,"urlLink") || (depth0 != null ? compilerNameLookup(depth0,"urlLink") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"urlLink","hash":{},"data":data}) : helper)))
    + " target=\"_blank\" class=\"sc-shippingbar-header-text-congratulations\">"
    + alias4(((helper = (helper = compilerNameLookup(helpers,"headerCongratsMessage") || (depth0 != null ? compilerNameLookup(depth0,"headerCongratsMessage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"headerCongratsMessage","hash":{},"data":data}) : helper)))
    + "</a>\n";
},"23":function(container,depth0,helpers,partials,data) {
    var helper;

  return "                                            <p class=\"sc-shippingbar-header-text-congratulations\">"
    + container.escapeExpression(((helper = (helper = compilerNameLookup(helpers,"headerCongratsMessage") || (depth0 != null ? compilerNameLookup(depth0,"headerCongratsMessage") : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"headerCongratsMessage","hash":{},"data":data}) : helper)))
    + "</p>\n";
},"25":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"isInProgress") : depth0),{"name":"if","hash":{},"fn":container.program(26, data, 0),"inverse":container.program(31, data, 0),"data":data})) != null ? stack1 : "");
},"26":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"haveLink") : depth0),{"name":"if","hash":{},"fn":container.program(27, data, 0),"inverse":container.program(29, data, 0),"data":data})) != null ? stack1 : "")
    + "\n";
},"27":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                                                <a href="
    + container.escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlLink") || (depth0 != null ? compilerNameLookup(depth0,"urlLink") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"urlLink","hash":{},"data":data}) : helper)))
    + " target=\"_blank\" class=\"sc-shippingbar-header-text-progress\">"
    + ((stack1 = ((helper = (helper = compilerNameLookup(helpers,"headerProgressMessage") || (depth0 != null ? compilerNameLookup(depth0,"headerProgressMessage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"headerProgressMessage","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\n";
},"29":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                                                <p class=\"sc-shippingbar-header-text-progress\">"
    + ((stack1 = ((helper = (helper = compilerNameLookup(helpers,"headerProgressMessage") || (depth0 != null ? compilerNameLookup(depth0,"headerProgressMessage") : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"headerProgressMessage","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</p>\n";
},"31":function(container,depth0,helpers,partials,data) {
    return "                                            <div class=\"sc-shippingbar-header-textlist-wrapper\">\n                                                <div data-view=\"ShippingBar.Header.Text\"></div>\n                                            </div>\n";
},"33":function(container,depth0,helpers,partials,data) {
    return "                                        <i class=\"sc-shippingbar-header-chevron-right\" data-action = \"next-text\"></i>\n";
},"35":function(container,depth0,helpers,partials,data) {
    return "                                    <div class=\"col-md-1 col-sm-1 col-xs-1 sc-shippingbar-header-close-container\">\n                                        <i class=\"sc-shippingbar-header-times-circle\" data-action=\"close-shipping-bar\"></i>\n                                    </div>\n";
},"37":function(container,depth0,helpers,partials,data) {
    return "sc-shippingbar-header-floating";
},"39":function(container,depth0,helpers,partials,data) {
    return "                                <i class=\"sc-shippingbar-header-chevron-left\" data-action=\"previous-text\"></i>\n";
},"41":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"haveLink") : depth0),{"name":"if","hash":{},"fn":container.program(42, data, 0),"inverse":container.program(44, data, 0),"data":data})) != null ? stack1 : "")
    + "\n";
},"42":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                                    <a a href="
    + container.escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlLink") || (depth0 != null ? compilerNameLookup(depth0,"urlLink") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"urlLink","hash":{},"data":data}) : helper)))
    + " target=\"_blank\" class=\"sc-shippingbar-header-text-congratulations\" id=\"header-congrats-message\">"
    + ((stack1 = ((helper = (helper = compilerNameLookup(helpers,"headerCongratsMessage") || (depth0 != null ? compilerNameLookup(depth0,"headerCongratsMessage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"headerCongratsMessage","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\n";
},"44":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                                    <p class=\"sc-shippingbar-header-text-congratulations\" id=\"header-congrats-message\">"
    + ((stack1 = ((helper = (helper = compilerNameLookup(helpers,"headerCongratsMessage") || (depth0 != null ? compilerNameLookup(depth0,"headerCongratsMessage") : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"headerCongratsMessage","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</p>\n";
},"46":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"isInProgress") : depth0),{"name":"if","hash":{},"fn":container.program(47, data, 0),"inverse":container.program(52, data, 0),"data":data})) != null ? stack1 : "");
},"47":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"haveLink") : depth0),{"name":"if","hash":{},"fn":container.program(48, data, 0),"inverse":container.program(50, data, 0),"data":data})) != null ? stack1 : "");
},"48":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                                        <a href="
    + container.escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlLink") || (depth0 != null ? compilerNameLookup(depth0,"urlLink") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"urlLink","hash":{},"data":data}) : helper)))
    + " target=\"_blank\" class=\"sc-shippingbar-header-text-progress\" id=\"header-progress-message\">"
    + ((stack1 = ((helper = (helper = compilerNameLookup(helpers,"headerProgressMessage") || (depth0 != null ? compilerNameLookup(depth0,"headerProgressMessage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"headerProgressMessage","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\n";
},"50":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                                        <p class=\"sc-shippingbar-header-text-progress\" id=\"header-progress-message\">"
    + ((stack1 = ((helper = (helper = compilerNameLookup(helpers,"headerProgressMessage") || (depth0 != null ? compilerNameLookup(depth0,"headerProgressMessage") : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"headerProgressMessage","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</p>\n";
},"52":function(container,depth0,helpers,partials,data) {
    return "                                    <div class=\"sc-shippingbar-header-textlist-wrapper\">\n                                        <div data-view=\"ShippingBar.Header.Text\"></div>\n                                    </div>\n";
},"54":function(container,depth0,helpers,partials,data) {
    return "                                <i class=\"sc-shippingbar-header-chevron-right\" data-action = \"next-text\"></i>\n";
},"56":function(container,depth0,helpers,partials,data) {
    return "                            <div class=\"col-md-1 col-sm-1 col-xs-1 sc-shippingbar-header-close-container\">\n                                <i class=\"sc-shippingbar-header-times-circle\" data-action=\"close-shipping-bar\"></i>\n                            </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<section>\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"shippingBarOpen") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</section>";
},"useData":true}; var main = t.main; t.main = function(){ arguments[1] = arguments[1] || {}; var ctx = arguments[1]; ctx._extension_path = 'http://localhost:7777/tmp/extensions/SuiteCommerce/ShippingBar/1.1.1/'; ctx._theme_path = 'http://localhost:7777/tmp/extensions/TavanoTeam/Manor/1.0.32/'; return main.apply(this, arguments); }; var template = Handlebars.template(t); template.Name = 'suitecommerce_shippingbar_header'; return template;});