define('suitecommerce_shippingbar_cart.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var t = {"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "        <div class=\"sc-shippingbar-cart-container\">\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"shippingGoalAvailable") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "        </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                <p class=\"sc-shippingbar-cart-message sc-shippingbar-cart-congratulations "
    + container.escapeExpression(((helper = (helper = compilerNameLookup(helpers,"device") || (depth0 != null ? compilerNameLookup(depth0,"device") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"device","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = compilerNameLookup(helpers,"cartCongratsMessage") || (depth0 != null ? compilerNameLookup(depth0,"cartCongratsMessage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"cartCongratsMessage","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</p>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"isInProgress") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data})) != null ? stack1 : "");
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                    <p class=\"sc-shippingbar-cart-message sc-shippingbar-cart-progress "
    + container.escapeExpression(((helper = (helper = compilerNameLookup(helpers,"device") || (depth0 != null ? compilerNameLookup(depth0,"device") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"device","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = compilerNameLookup(helpers,"cartProgressMessage") || (depth0 != null ? compilerNameLookup(depth0,"cartProgressMessage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"cartProgressMessage","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</p>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                    <p class=\"sc-shippingbar-cart-message sc-shippingbar-cart-initial "
    + container.escapeExpression(((helper = (helper = compilerNameLookup(helpers,"device") || (depth0 != null ? compilerNameLookup(depth0,"device") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"device","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = compilerNameLookup(helpers,"cartInitialMessage") || (depth0 != null ? compilerNameLookup(depth0,"cartInitialMessage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"cartInitialMessage","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<section data-view=\"Cart.Shipping.Bar\" >\n"
    + ((stack1 = compilerNameLookup(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? compilerNameLookup(depth0,"showBanner") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</section>";
},"useData":true}; var main = t.main; t.main = function(){ arguments[1] = arguments[1] || {}; var ctx = arguments[1]; ctx._extension_path = 'http://localhost:7777/tmp/extensions/SuiteCommerce/ShippingBar/1.1.1/'; ctx._theme_path = 'http://localhost:7777/tmp/extensions/TavanoTeam/Manor/1.0.32/'; return main.apply(this, arguments); }; var template = Handlebars.template(t); template.Name = 'suitecommerce_shippingbar_cart'; return template;});