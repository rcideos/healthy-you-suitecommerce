{{#if showButton}}
    {{#if isButtonBottom}}
      <div class="col-md-3">
      </div>
      <div class="col-md-9">
        <div class="infinite-scroll-btn-holder arrow-down">
            <a data-action="load-pages" class="infinite-scroll-btn-load-page arrow-down" id="{{ buttonID }}">
                <p>{{ buttonText }}<p>
                <i class="icon-chevron-down arrow-down"></i>
            </a>
        </div>
      </div>
    {{else}}
        <div class="infinite-scroll-btn-holder arrow-up">
          <a data-action="load-pages" class="infinite-scroll-btn-load-page arrow-up" id="{{ buttonID }}">
              <i class="icon-chevron-up arrow-up"></i>
              <p>{{ buttonText }}<p>
          </a>
        </div>
    {{/if}}
{{/if}}
