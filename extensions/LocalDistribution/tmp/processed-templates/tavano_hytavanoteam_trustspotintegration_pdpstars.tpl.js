define('tavano_hytavanoteam_trustspotintegration_pdpstars.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var t = {"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "\r\n<div class=\"trustspot-inline-product\" data-product-sku=\""
    + container.escapeExpression(((helper = (helper = compilerNameLookup(helpers,"productsku") || (depth0 != null ? compilerNameLookup(depth0,"productsku") : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"productsku","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n\r\n";
},"useData":true}; var main = t.main; t.main = function(){ arguments[1] = arguments[1] || {}; var ctx = arguments[1]; ctx._extension_path = 'http://localhost:7777/tmp/extensions/Tavano/HYTavanoTeam/1.0.0/'; ctx._theme_path = 'http://localhost:7777/tmp/extensions/TavanoTeam/Manor/1.0.32/'; return main.apply(this, arguments); }; var template = Handlebars.template(t); template.Name = 'tavano_hytavanoteam_trustspotintegration_pdpstars'; return template;});