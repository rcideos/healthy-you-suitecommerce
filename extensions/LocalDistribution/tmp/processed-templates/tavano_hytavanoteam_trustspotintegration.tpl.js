define('tavano_hytavanoteam_trustspotintegration.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var t = {"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"trustspot trustspot-main-widget\"\n		data-product-sku=\""
    + alias4(((helper = (helper = compilerNameLookup(helpers,"productsku") || (depth0 != null ? compilerNameLookup(depth0,"productsku") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"productsku","hash":{},"data":data}) : helper)))
    + "\" \n		data-name=\""
    + alias4(((helper = (helper = compilerNameLookup(helpers,"productname") || (depth0 != null ? compilerNameLookup(depth0,"productname") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"productname","hash":{},"data":data}) : helper)))
    + "\">\n</div>\n\n";
},"useData":true}; var main = t.main; t.main = function(){ arguments[1] = arguments[1] || {}; var ctx = arguments[1]; ctx._extension_path = 'http://localhost:7777/tmp/extensions/Tavano/HYTavanoTeam/1.0.0/'; ctx._theme_path = 'http://localhost:7777/tmp/extensions/TavanoTeam/Manor/1.0.32/'; return main.apply(this, arguments); }; var template = Handlebars.template(t); template.Name = 'tavano_hytavanoteam_trustspotintegration'; return template;});