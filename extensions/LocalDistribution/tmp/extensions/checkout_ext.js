var extensions = {};

extensions['Tavano.HYTavanoTeam.1.0.0'] = function(){

function getExtensionAssetsPath(asset){
	return 'extensions/Tavano/HYTavanoTeam/1.0.0/' + asset;
}

define(
	'Tavano.HYTavanoTeam.TrustSpotIntegration'
,   [
		'TrustSpotIntegration.View'
	,	'jQuery'
	]
,   function (
		TrustSpotIntegrationView
	,	jQuery
	)
{
	'use strict';
	  

	return  {

		scriptLoaded : false

		 ,loadTrustSpotInit : function (){

		 	this.scriptLoaded && trustspot_init()
		 }


		 ,loadTrustSpotScript : function (container) {

		  	var self = this;
		  	 window.trustspot_key = '42dc2a7ac537dfa6e3353723c79d1d080f08e487aec19b3d14b695b07052a2aec4ae10c2b9a5d267d8c63401a72ad31a00fdcf3554db3068fbf584c0b7b7ef42';

		  	 var link = document.createElement( 'link' );
  			 link.setAttribute( 'src', 'https://trustspot.io/index.php/api/pub/product_widget_css/5474/widget.css' );
  			 link.setAttribute( 'rel', 'stylesheet' );
  			 link.setAttribute( 'type', 'text/css' );
  			 document.head.appendChild( link );

  			 window.globalTrustSpotURL="https://trustspot.io";

             jQuery.getScript("https://trustspot.io/assets/js/trustspot_product_reviews.js", function (data, textStatus, jqxhr) {
              
			   trustspot_init();
			   self.scriptLoaded = true
			 });

        }
	};
});



define(
	'Tavano.HYTavanoTeam.ShopperApprovedIntegration'
,   [
		'Tracker'
		,'LiveOrder.Model'
		,'Profile.Model'
	]
,   function (
		Tracker
		,LiveOrder
		,ProfileModel
	)
{
	'use strict';

	var shopperApprover =  {
		trackTransaction: function (confirmation)
            {
            	
                var product_list = [];
                try{

                    confirmation.get('products').each(function (product)
                    {
                        product_list.push(
                            { "id": product.get('name'), 
                            "price": product.get('rate'), 
                            "quantity": product.get('quantity') }
                        );
                    })
                    var tran_id = LiveOrder.getInstance().get("confirmation").confirmationnumber;
                    var fullname = ProfileModel.getInstance().get("firstname")+" "+ProfileModel.getInstance().attributes.lastname;
                    var email = ProfileModel.getInstance().get("email");
                    var country = LiveOrder.getInstance().get("addresses").models[0].attributes.country;
                    var state = LiveOrder.getInstance().get("addresses").models[0].attributes.state;
					jQuery('head').prepend('<script type="text/javascript"> var sa_values = {      "site":22964,      "token":"QfkTp2gmbCNFwh3",     "orderid":"'+tran_id+'",     "name":"'+fullname+'",     "email":"'+email+'",     "country":"'+country+'",     "state":"'+state+'"}; function saLoadScript(src) {      var js = window.document.createElement("script");     js.src = src; js.type = "text/javascript";     document .getElementsByTagName("head")[0].appendChild(js);}     var d = new Date();     if (d.getTime() - 172800000 > 1477399567000)         saLoadScript("//www.shopperapproved.com/thankyou/rate/22964.js");     else         saLoadScript("//direct.shopperapproved.com/thankyou/rate/22964.js?d=" + d.getTime()); </script> ');


                }catch(e){
                    console.log("Error on track Criteo Track transaction")
                }
                return this;
            },
        

		mountToApp: function mountToApp (container)
		{
			// using the 'Layout' component we add a new child view inside the 'Header' existing view 
			// (there will be a DOM element with the HTML attribute data-view="Header.Logo")
			// more documentation of the Extensibility API in
			// https://system.netsuite.com/help/helpcenter/en_US/APIs/SuiteCommerce/Extensibility/Frontend/index.html
			
			/** @type {LayoutComponent} */
			'use strict';

			Tracker.getInstance().trackers.push(shopperApprover);
        	
          
           
		}
	};

	return shopperApprover
});


// @module Tavano.HYTavanoTeam.ShopperApprovedIntegration
define('TrustSpotIntegration.View'
,	[
		'tavano_hytavanoteam_trustspotintegration.tpl'
	,	'Utils'
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	]
,	function (
		tavano_hytavanoteam_trustspotintegration_tpl
	,	Utils
	,	Backbone
	,	jQuery
	,	_
	)
{
	'use strict';

	// @class Tavano.HYTavanoTeam.ShopperApprovedIntegration.View @extends Backbone.View
	return Backbone.View.extend({

		template: tavano_hytavanoteam_trustspotintegration_tpl

	,	initialize: function (options) {

			/*  Uncomment to test backend communication with an example service 
				(you'll need to deploy and activate the extension first)
			*/
			this.itemInfo = options.pdp.getItemInfo();
			this.message = '';

		}

	,	events: {
		}

	,	bindings: {
		}

	, 	childViews: {
			
		}

		//@method getContext @return Tavano.HYTavanoTeam.ShopperApprovedIntegration.View.Context
	,	getContext: function getContext()
		{	
			//@class Tavano.HYTavanoTeam.ShopperApprovedIntegration.View.Context
			this.message = this.message || 'Hello World!!'
			return {
				message: this.message,
				productsku: this.itemInfo.item.keyMapping_sku ,
				productname: this.itemInfo.item.keyMapping_name
			};
		}
	});
});

define(
	'NavigationHelper.Extension'
,   [
		'jQuery'
	,	'NavigationHelper'
	,	'underscore'
	]
,   function (
		jQuery
	,	NavigationHelper
	,	_
	)
{
	return  {
		mountToApp: function mountToApp (container)
		{
			var Layout = container.getLayout();

			_.extend(Layout,{

				/* This method allows you to control the "click" event in the DOM*/
				 executeClick: function(e) {
				 	
				 	/* These parameters were changed because they generated unexpected behaviour in the SPA*/
                	if (e && e.target && e.target.href=="javascript:void(0)"){
                		e.target.href="#"
                	}
                	else if (e && e.target && e.target.href.includes("trustspot-widget-wrapper")){
                		e.target.href="#"
                	}

                	this.click.executeAll(e);
            }
			})

		}
	}
	

})

// @module Tavano.HYTavanoTeam.ShopperApprovedIntegration
define('TrustSpotIntegration.PDPstars.View'
,	[
		'tavano_hytavanoteam_trustspotintegration_pdpstars.tpl'
	,	'Utils'
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	]
,	function (
		tavano_hytavanoteam_trustspotintegration_pdpstars_tpl
	,	Utils
	,	Backbone
	,	jQuery
	,	_
	)
{
	'use strict';

	// @class Tavano.HYTavanoTeam.ShopperApprovedIntegration.View @extends Backbone.View
	return Backbone.View.extend({

		template: tavano_hytavanoteam_trustspotintegration_pdpstars_tpl

	,	initialize: function (options) {

			/*  Uncomment to test backend communication with an example service 
				(you'll need to deploy and activate the extension first)
			*/
			this.itemInfo = options.pdp.getItemInfo();
			this.message = '';

		}

	,	events: {
		}

	,	bindings: {
		}

	, 	childViews: {
			
		}

		//@method getContext @return Tavano.HYTavanoTeam.ShopperApprovedIntegration.View.Context
	,	getContext: function getContext()
		{	
			//@class Tavano.HYTavanoTeam.ShopperApprovedIntegration.View.Context
        this.message = this.message || ''

			return {
				message: this.message,
				productsku: this.itemInfo.item.keyMapping_sku ,
				productname: this.itemInfo.item.keyMapping_name
			};
		}
	});
});

define('LoginRegister.Register.View.Extension'
    , [
        'Backbone'
        , 'user_categories_registration_loginregister.tpl'
    ]
    , function
        (
        Backbone
        , user_categories_registration_loginregister_tpl
        ) {
        'use strict';

        return Backbone.View.extend({
            template: user_categories_registration_loginregister_tpl
            , initialize: function (options) {
                var LoginRegisterPage = options.container.getComponent('LoginRegisterPage');
                var UserProfile = options.container.getComponent('UserProfile');
                this.options = options;
                this.UserProfile = UserProfile
            }
            , getContext: function () {
                var categories = _.find(this.UserProfile.customfields, function (field) {
                    return field.id == 'custentity_tt_user_register_categories'
                });

                return {
                    categories: !!categories
                }
            }
        });
    });

/*
	� 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module LoginRegister
define('LoginRegister.Extension'
    , [
        'login_register_register.tpl'
        , 'LoginRegister.Register.View.Extension'
        , 'SC.Configuration'
        , 'LoginRegister.Utils'
        , 'Tracker'
        , 'Profile.Model'
        , 'LiveOrder.Model'
        , 'Backbone.FormView'

        , 'Backbone'
        , 'underscore'
        , 'Utils'
    ]
    , function (
        register_tpl
        , LoginRegisterViewExtension
        , Configuration
        , LoginRegisterUtils
        , Tracker
        , ProfileModel
        , LiveOrderModel
        , BackboneFormView

        , Backbone
        , _
    ) {
        'use strict';
        
        return {

            mountToApp: function mountToApp(container) {
                var LoginRegisterPage = container.getComponent('LoginRegisterPage');
                if (LoginRegisterPage) {
                    LoginRegisterPage.addChildView('Register.CustomFields', function () {
                        return new LoginRegisterViewExtension
                            ({
                                container: container
                            })
                    });
                }
            }
        };
    });


define(
	'Tavano.HYTavanoTeam.HYTavanoTeam'
,   [
		'Tavano.HYTavanoTeam.TrustSpotIntegration'
		,'TrustSpotIntegration.View'
		,'TrustSpotIntegration.PDPstars.View'
		,'Tavano.HYTavanoTeam.ShopperApprovedIntegration'
        ,'NavigationHelper.Extension'
        ,'LoginRegister.Extension'

	]
,   function (
		HYTavanoTeamTrustSpotIntegration
		,TrustSpotIntegrationView
		,TrustSpotIntegrationPDPstarsView
		,TavanoHYTavanoTeamShopperApprovedIntegration
        ,NavigationHelperExtension
        ,LoginRegisterExtension
	)
{
	'use strict';
	
	/* Here we define the entry point of the Extension*/
	return  {
		mountToApp: function mountToApp (container)
		{
			/* Here we add the needed modules to the entry point mountToApp*/
			TavanoHYTavanoTeamShopperApprovedIntegration.mountToApp(container);
            NavigationHelperExtension.mountToApp(container);
            
			if (SC.ENVIRONMENT.jsEnvironment === "browser") {

				var self = this,
                    layout = container.getLayout();

                layout.once("afterAppendView", function (view) {

                    HYTavanoTeamTrustSpotIntegration.loadTrustSpotScript(container)
                });

                layout.on("afterAppendView", function (view) {

                     HYTavanoTeamTrustSpotIntegration.loadTrustSpotInit()
                });

			}
			
			var layout = container.getLayout();


			var pdp = container.getComponent('PDP'),
                plp = container.getComponent('PLP');

			/* Here we add the Trustspot Reviews widget to the needed data-view*/
            if (pdp)
            {
			pdp.addChildViews(
    		pdp.PDP_FULL_VIEW 
			  ,{
			      'ProductReviews.Center': {
			        'ProductReviews.Center': {
			          childViewIndex: 1
			        ,   childViewConstructor: function(){
			            return new TrustSpotIntegrationView({pdp:pdp});
			          }
			        }
      			}
    			}
  			)


			/* Here we add the Inline Stars div to the needed data-view*/
  			pdp.addChildViews(
    		pdp.PDP_FULL_VIEW 
			  ,{
			      'Global.StarRating': {
			        'Global.StarRating': {
			          childViewIndex: 1
			        ,   childViewConstructor: function(){
			            return new TrustSpotIntegrationPDPstarsView({pdp:pdp});
			          }
			        }
      			}
    			}
  			)

			}

			

			/* We make sure that everytime the user loads a new page in the PLP, 
			the event that triggers the trustspot inline-stars div is called*/

			if(plp)
			{
			plp.on("InfiniteScroll-afterRenderPage",function(){HYTavanoTeamTrustSpotIntegration.loadTrustSpotInit()})
            }

            var checkout = container.getComponent('Checkout');

            if (checkout) {
                LoginRegisterExtension.mountToApp(container)
            }
		}
	};
});


};


try{
	extensions['Tavano.HYTavanoTeam.1.0.0']();
	SC.addExtensionModule('Tavano.HYTavanoTeam.HYTavanoTeam');
}
catch(error)
{
	console.error(error);
}

