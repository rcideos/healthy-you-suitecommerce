define(
	'Tavano.HYTavanoTeam.TrustSpotIntegration'
,   [
		'TrustSpotIntegration.View'
	,	'jQuery'
	]
,   function (
		TrustSpotIntegrationView
	,	jQuery
	)
{
	'use strict';
	  

	return  {

		scriptLoaded : false

		 ,loadTrustSpotInit : function (){

		 	this.scriptLoaded && trustspot_init()
		 }


		 ,loadTrustSpotScript : function (container) {

		  	var self = this;
		  	 window.trustspot_key = '42dc2a7ac537dfa6e3353723c79d1d080f08e487aec19b3d14b695b07052a2aec4ae10c2b9a5d267d8c63401a72ad31a00fdcf3554db3068fbf584c0b7b7ef42';

		  	 var link = document.createElement( 'link' );
  			 link.setAttribute( 'src', 'https://trustspot.io/index.php/api/pub/product_widget_css/5474/widget.css' );
  			 link.setAttribute( 'rel', 'stylesheet' );
  			 link.setAttribute( 'type', 'text/css' );
  			 document.head.appendChild( link );

  			 window.globalTrustSpotURL="https://trustspot.io";

             jQuery.getScript("https://trustspot.io/assets/js/trustspot_product_reviews.js", function (data, textStatus, jqxhr) {
              
			   trustspot_init();
			   self.scriptLoaded = true
			 });

        }
	};
});
