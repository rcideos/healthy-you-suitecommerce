// @module Tavano.HYTavanoTeam.ShopperApprovedIntegration
define('TrustSpotIntegration.PDPstars.View'
,	[
		'tavano_hytavanoteam_trustspotintegration_pdpstars.tpl'
	,	'Utils'
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	]
,	function (
		tavano_hytavanoteam_trustspotintegration_pdpstars_tpl
	,	Utils
	,	Backbone
	,	jQuery
	,	_
	)
{
	'use strict';

	// @class Tavano.HYTavanoTeam.ShopperApprovedIntegration.View @extends Backbone.View
	return Backbone.View.extend({

		template: tavano_hytavanoteam_trustspotintegration_pdpstars_tpl

	,	initialize: function (options) {

			/*  Uncomment to test backend communication with an example service 
				(you'll need to deploy and activate the extension first)
			*/
			this.itemInfo = options.pdp.getItemInfo();
			this.message = '';

		}

	,	events: {
		}

	,	bindings: {
		}

	, 	childViews: {
			
		}

		//@method getContext @return Tavano.HYTavanoTeam.ShopperApprovedIntegration.View.Context
	,	getContext: function getContext()
		{	
			//@class Tavano.HYTavanoTeam.ShopperApprovedIntegration.View.Context
        this.message = this.message || ''

			return {
				message: this.message,
				productsku: this.itemInfo.item.keyMapping_sku ,
				productname: this.itemInfo.item.keyMapping_name
			};
		}
	});
});