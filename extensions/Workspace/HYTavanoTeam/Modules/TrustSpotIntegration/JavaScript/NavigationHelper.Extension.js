define(
	'NavigationHelper.Extension'
,   [
		'jQuery'
	,	'NavigationHelper'
	,	'underscore'
	]
,   function (
		jQuery
	,	NavigationHelper
	,	_
	)
{
	return  {
		mountToApp: function mountToApp (container)
		{
			var Layout = container.getLayout();

			_.extend(Layout,{

				/* This method allows you to control the "click" event in the DOM*/
				 executeClick: function(e) {
				 	
				 	/* These parameters were changed because they generated unexpected behaviour in the SPA*/
                	if (e && e.target && e.target.href=="javascript:void(0)"){
                		e.target.href="#"
                	}
                	else if (e && e.target && e.target.href.includes("trustspot-widget-wrapper")){
                		e.target.href="#"
                	}

                	this.click.executeAll(e);
            }
			})

		}
	}
	

})