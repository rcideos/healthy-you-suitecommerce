
define(
	'Tavano.HYTavanoTeam.HYTavanoTeam'
,   [
		'Tavano.HYTavanoTeam.TrustSpotIntegration'
		,'TrustSpotIntegration.View'
		,'TrustSpotIntegration.PDPstars.View'
		,'Tavano.HYTavanoTeam.ShopperApprovedIntegration'
        ,'NavigationHelper.Extension'
        ,'LoginRegister.Extension'

	]
,   function (
		HYTavanoTeamTrustSpotIntegration
		,TrustSpotIntegrationView
		,TrustSpotIntegrationPDPstarsView
		,TavanoHYTavanoTeamShopperApprovedIntegration
        ,NavigationHelperExtension
        ,LoginRegisterExtension
	)
{
	'use strict';
	
	/* Here we define the entry point of the Extension*/
	return  {
		mountToApp: function mountToApp (container)
		{
			/* Here we add the needed modules to the entry point mountToApp*/
			TavanoHYTavanoTeamShopperApprovedIntegration.mountToApp(container);
            NavigationHelperExtension.mountToApp(container);
            
			if (SC.ENVIRONMENT.jsEnvironment === "browser") {

				var self = this,
                    layout = container.getLayout();

                layout.once("afterAppendView", function (view) {

                    HYTavanoTeamTrustSpotIntegration.loadTrustSpotScript(container)
                });

                layout.on("afterAppendView", function (view) {

                     HYTavanoTeamTrustSpotIntegration.loadTrustSpotInit()
                });

			}
			
			var layout = container.getLayout();


			var pdp = container.getComponent('PDP'),
                plp = container.getComponent('PLP');

			/* Here we add the Trustspot Reviews widget to the needed data-view*/
            if (pdp)
            {
			pdp.addChildViews(
    		pdp.PDP_FULL_VIEW 
			  ,{
			      'ProductReviews.Center': {
			        'ProductReviews.Center': {
			          childViewIndex: 1
			        ,   childViewConstructor: function(){
			            return new TrustSpotIntegrationView({pdp:pdp});
			          }
			        }
      			}
    			}
  			)


			/* Here we add the Inline Stars div to the needed data-view*/
  			pdp.addChildViews(
    		pdp.PDP_FULL_VIEW 
			  ,{
			      'Global.StarRating': {
			        'Global.StarRating': {
			          childViewIndex: 1
			        ,   childViewConstructor: function(){
			            return new TrustSpotIntegrationPDPstarsView({pdp:pdp});
			          }
			        }
      			}
    			}
  			)

			}

			

			/* We make sure that everytime the user loads a new page in the PLP, 
			the event that triggers the trustspot inline-stars div is called*/

			if(plp)
			{
			plp.on("InfiniteScroll-afterRenderPage",function(){HYTavanoTeamTrustSpotIntegration.loadTrustSpotInit()})
            }

            var checkout = container.getComponent('Checkout');

            if (checkout) {
                LoginRegisterExtension.mountToApp(container)
            }
		}
	};
});
