
define(
	'Tavano.HYTavanoTeam.ShopperApprovedIntegration'
,   [
		'Tracker'
		,'LiveOrder.Model'
		,'Profile.Model'
	]
,   function (
		Tracker
		,LiveOrder
		,ProfileModel
	)
{
	'use strict';

	var shopperApprover =  {
		trackTransaction: function (confirmation)
            {
            	
                var product_list = [];
                try{

                    confirmation.get('products').each(function (product)
                    {
                        product_list.push(
                            { "id": product.get('name'), 
                            "price": product.get('rate'), 
                            "quantity": product.get('quantity') }
                        );
                    })
                    var tran_id = LiveOrder.getInstance().get("confirmation").confirmationnumber;
                    var fullname = ProfileModel.getInstance().get("firstname")+" "+ProfileModel.getInstance().attributes.lastname;
                    var email = ProfileModel.getInstance().get("email");
                    var country = LiveOrder.getInstance().get("addresses").models[0].attributes.country;
                    var state = LiveOrder.getInstance().get("addresses").models[0].attributes.state;
					jQuery('head').prepend('<script type="text/javascript"> var sa_values = {      "site":22964,      "token":"QfkTp2gmbCNFwh3",     "orderid":"'+tran_id+'",     "name":"'+fullname+'",     "email":"'+email+'",     "country":"'+country+'",     "state":"'+state+'"}; function saLoadScript(src) {      var js = window.document.createElement("script");     js.src = src; js.type = "text/javascript";     document .getElementsByTagName("head")[0].appendChild(js);}     var d = new Date();     if (d.getTime() - 172800000 > 1477399567000)         saLoadScript("//www.shopperapproved.com/thankyou/rate/22964.js");     else         saLoadScript("//direct.shopperapproved.com/thankyou/rate/22964.js?d=" + d.getTime()); </script> ');


                }catch(e){
                    console.log("Error on track Criteo Track transaction")
                }
                return this;
            },
        

		mountToApp: function mountToApp (container)
		{
			// using the 'Layout' component we add a new child view inside the 'Header' existing view 
			// (there will be a DOM element with the HTML attribute data-view="Header.Logo")
			// more documentation of the Extensibility API in
			// https://system.netsuite.com/help/helpcenter/en_US/APIs/SuiteCommerce/Extensibility/Frontend/index.html
			
			/** @type {LayoutComponent} */
			'use strict';

			Tracker.getInstance().trackers.push(shopperApprover);
        	
          
           
		}
	};

	return shopperApprover
});
