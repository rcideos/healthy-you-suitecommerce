
function service(request, response)
{
	'use strict';
	try 
	{
		require('Tavano.HYTavanoTeam.HYTavanoTeam.ServiceController').handle(request, response);
	} 
	catch(ex)
	{
		console.log('Tavano.HYTavanoTeam.HYTavanoTeam.ServiceController ', ex);
		var controller = require('ServiceController');
		controller.response = response;
		controller.request = request;
		controller.sendError(ex);
	}
}